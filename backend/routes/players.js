const router = require('express').Router();
let Player = require('../models/player.model'); //mongoose model

//first endpoint
router.route('/').get((req, res) => {
	Player.find() //mongoose method that get all the players in the mongoDB database
		.then((players) => res.json(players)) //return players in json format
		.catch((err) => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
	const name = req.body.name;
	const team = req.body.team;
	const position = req.body.position;
	const age = req.body.age;
	const country = req.body.country;

	const newPlayer = new Player({ name, team, position, age, country });

	newPlayer
		.save() // new player is save to the database
		.then(() => res.json('Player Added'))
		.catch((err) => res.status(400).json('Error: ' + err));
});

router.route('/:id').get((req, res) => {
	Player.findById(req.params.id)
		.then((player) => res.json(player))
		.catch((err) => res.status(400).json('Error: ' + err));
});

router.route('/:id').delete((req, res) => {
	Player.findByIdAndDelete(req.params.id)
		.then(() => res.json('Player deleted'))
		.catch((err) => res.status(400).json('Error ' + err));
});

router.route('/update/:id').post((req, res) => {
	Player.findById(req.params.id)
		.then((player) => {
			player.name = req.body.name;
			player.team = req.body.team;
			player.position = req.body.position;
			player.age = Number(req.body.age);
			player.country = req.body.country;

			player
				.save()
				.then(() => res.json('Player updated'))
				.catch((err) => res.status(400).json('Error: ' + err));
		})
		.catch((err) => res.status(400).json('Erro: ' + err));
});

router.route('/find/team/sortup').get((req, res) => {
	Player.find()
		.sort({ team: -1 })
		.then((players) => res.json(players)) //return players in json format
		.catch((err) => res.status(400).json('Error: ' + err));
});

router.route('/find/team/sortdown').get((req, res) => {
	Player.find()
		.sort({ team: 1 })
		.then((players) => res.json(players)) //return players in json format
		.catch((err) => res.status(400).json('Error: ' + err));
});

router.route('/find/name/sortup').get((req, res) => {
	Player.find()
		.sort({ name: -1 })
		.then((players) => res.json(players)) //return players in json format
		.catch((err) => res.status(400).json('Error: ' + err));
});

router.route('/find/name/sortdown').get((req, res) => {
	Player.find()
		.sort({ name: 1 })
		.then((players) => res.json(players)) //return players in json format
		.catch((err) => res.status(400).json('Error: ' + err));
});

router.route('/find/position/sortup').get((req, res) => {
	Player.find()
		.sort({ position: -1 })
		.then((players) => res.json(players)) //return players in json format
		.catch((err) => res.status(400).json('Error: ' + err));
});

router.route('/find/position/sortdown').get((req, res) => {
	Player.find()
		.sort({ position: 1 })
		.then((players) => res.json(players)) //return players in json format
		.catch((err) => res.status(400).json('Error: ' + err));
});

router.route('/find/sort/age/descending').get((req, res) => {
	Player.find()
		.sort({ age: -1 })
		.then((players) => res.json(players)) //return players in json format
		.catch((err) => res.status(400).json('Error: ' + err));
});

router.route('/find/sort/age/ascending').get((req, res) => {
	Player.find()
		.sort({ age: 1 })
		.then((players) => res.json(players)) //return players in json format
		.catch((err) => res.status(400).json('Error: ' + err));
});

router.route('/find/country/sortup').get((req, res) => {
	Player.find()
		.sort({ country: -1 })
		.then((players) => res.json(players)) //return players in json format
		.catch((err) => res.status(400).json('Error: ' + err));
});

router.route('/find/country/sortdown').get((req, res) => {
	Player.find()
		.sort({ country: 1 })
		.then((players) => res.json(players)) //return players in json format
		.catch((err) => res.status(400).json('Error: ' + err));
});

router.route('/find/createdAt/sortup').get((req, res) => {
	Player.find()
		.sort({ createdAt: -1 })
		.then((players) => res.json(players)) //return players in json format
		.catch((err) => res.status(400).json('Error: ' + err));
});

router.route('/find/createdAt/sortdown').get((req, res) => {
	Player.find()
		.sort({ createdAt: 1 })
		.then((players) => res.json(players)) //return players in json format
		.catch((err) => res.status(400).json('Error: ' + err));
});

//filter teams
router.route('/find/:team').get((req, res) => {
	Player.find({ team: req.params.team })
		.then((players) => res.json(players)) //return players in json format
		.catch((err) => res.status(400).json('Error: ' + err));
});

module.exports = router;
