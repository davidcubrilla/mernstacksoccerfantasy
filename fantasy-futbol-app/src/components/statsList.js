import React, { Component } from 'react';
import Axios from 'axios';
import { PlayerStat } from './stat';

export class StatsList extends Component {
	constructor(props) {
		super(props);

		this.state = {
			stats: [],
			goalsSort: false,
			assistsSort: false,
			redCardsSort: false,
			yellowCardsSort: false,
		};

		this.updateStats = this.updateStats.bind(this);
	}

	//will make axios call after the first render
	componentDidMount() {
		Axios.get('http://localhost:5000/stats')
			.then((response) => {
				this.setState({ stats: response.data });
			})
			.catch((error) => {
				console.log(error);
			});
	}

	updateStats() {
		Axios.get('http://localhost:5000/stats')
			.then((response) => {
				this.setState({ stats: response.data });
			})
			.catch((error) => {
				console.log(error);
			});
	}

	goalsAscending() {
		Axios.get('http://localhost:5000/stats//find/sort/goals/ascending')
			.then((response) => {
				this.setState({ stats: response.data });
			})
			.catch((error) => {
				console.log(error);
			});
	}

	goalsDescending() {
		Axios.get('http://localhost:5000/stats//find/sort/goals/descending')
			.then((response) => {
				this.setState({ stats: response.data });
			})
			.catch((error) => {
				console.log(error);
			});
	}

	assistsAscending() {
		Axios.get('http://localhost:5000/stats//find/sort/assists/ascending')
			.then((response) => {
				this.setState({ stats: response.data });
			})
			.catch((error) => {
				console.log(error);
			});
	}

	assistsDescending() {
		Axios.get('http://localhost:5000/stats//find/sort/assists/descending')
			.then((response) => {
				this.setState({ stats: response.data });
			})
			.catch((error) => {
				console.log(error);
			});
	}

	redCardsAscending() {
		Axios.get('http://localhost:5000/stats//find/sort/redCards/ascending')
			.then((response) => {
				this.setState({ stats: response.data });
			})
			.catch((error) => {
				console.log(error);
			});
	}

	redCardsDescending() {
		Axios.get('http://localhost:5000/stats//find/sort/redCards/descending')
			.then((response) => {
				this.setState({ stats: response.data });
			})
			.catch((error) => {
				console.log(error);
			});
	}

	yellowCardsAscending() {
		Axios.get('http://localhost:5000/stats//find/sort/yellowCards/ascending')
			.then((response) => {
				this.setState({ stats: response.data });
			})
			.catch((error) => {
				console.log(error);
			});
	}

	yellowCardsDescending() {
		Axios.get('http://localhost:5000/stats//find/sort/yellowCards/descending')
			.then((response) => {
				this.setState({ stats: response.data });
			})
			.catch((error) => {
				console.log(error);
			});
	}

	statsList() {
		return this.state.stats.map((currentStat) => {
			return (
				<PlayerStat
					key={currentStat._id}
					stat={currentStat}
					updateStats={this.updateStats}
				/>
			);
		});
	}

	handleSortGoals() {
		if (this.state.goalsSort) {
			this.goalsAscending();
			this.setState({
				goalsSort: false,
			});
		} else {
			this.goalsDescending();
			this.setState({
				goalsSort: true,
			});
		}
	}

	handleSortAssists() {
		if (this.state.assistsSort) {
			this.assistsAscending();
			this.setState({
				assistsSort: false,
			});
		} else {
			this.assistsDescending();
			this.setState({
				assistsSort: true,
			});
		}
	}

	handleSortRedCards() {
		if (this.state.redCardsSort) {
			this.redCardsAscending();
			this.setState({
				redCardsSort: false,
			});
		} else {
			this.redCardsDescending();
			this.setState({
				redCardsSort: true,
			});
		}
	}

	handleSortYellowCards() {
		if (this.state.yellowCardsSort) {
			this.yellowCardsAscending();
			this.setState({
				yellowCardsSort: false,
			});
		} else {
			this.yellowCardsDescending();
			this.setState({
				yellowCardsSort: true,
			});
		}
	}

	render() {
		let stats = this.state.stats;
		//have to check if stats array is empty because it will have an error when rendering
		return stats.length !== 0 ? (
			<div>
				<h3>Stats</h3>
				<button
					onClick={() => {
						this.props.unrender();
					}}
				>
					Players' Info
				</button>
				<table className="table">
					<thead className="thead-light">
						<tr>
							<th>Player</th>
							<th>
								<button
									onClick={() => {
										this.handleSortGoals();
									}}
								>
									Goals
								</button>
							</th>
							<th>
								<button
									onClick={() => {
										this.handleSortAssists();
									}}
								>
									Assists
								</button>
							</th>
							<th>
								<button
									onClick={() => {
										this.handleSortRedCards();
									}}
								>
									Red Cards
								</button>
							</th>
							<th>
								<button
									onClick={() => {
										this.handleSortYellowCards();
									}}
								>
									Yellow Cards
								</button>
							</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>{this.statsList()}</tbody>
				</table>
			</div>
		) : (
			<div>'loading stats or no stats available'</div>
		);
	}
}
