import React, { Component } from 'react';
import Axios from 'axios';

export class PlayerStat extends Component {
	constructor(props) {
		super(props);

		this.state = {
			statKey: this.props.stat.statKey,
			reload: 0,
		};

		this.handleClear = this.handleClear.bind(this);
	}

	handleClear() {
		Axios.post(`http://localhost:5000/stats/update/${this.props.stat._id}`, {
			statKey: this.state.statKey,
			playerName: this.props.playerName,
			goals: 0,
			assists: 0,
			redCards: 0,
			yellowCards: 0,
		}).then(() => {
			this.props.updateStats();
		});
	}

	render() {
		return (
			<tr>
				<td>{this.props.stat.playerName}</td>
				<td>{this.props.stat.goals}</td>
				<td>{this.props.stat.assists}</td>
				<td>{this.props.stat.redCards}</td>
				<td>{this.props.stat.yellowCards}</td>
				<td>
					<button
						onClick={() => {
							this.handleClear();
						}}
					>
						Clear
					</button>
				</td>
			</tr>
		);
	}
}
